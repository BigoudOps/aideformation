Le PDF qui est fourni ici a pour but d'aider toute personne voulant se former.
Je fournis le fichier base.odt afin que chacun puisse contribuer avec ses propres méthodes d'apprentissage.
N'hésitez pas à soumettre vos idées ainsi que vos propres méthodes, vous pouvez si vous le désirez signer votre proposition avec votre nom et vos différents contacts.
Ce PDF reste open-source donc bien sûr vous êtes libre de le modifier a votre conveance et de le partager. 